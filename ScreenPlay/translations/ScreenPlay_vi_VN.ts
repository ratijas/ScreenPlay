<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="vi_VN" sourcelanguage="en">
<context>
    <name>Community</name>
    <message>
        <source>News</source>
        <translation>Tin tức</translation>
    </message>
    <message>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <source>Forum</source>
        <translation>Diễn đàn</translation>
    </message>
    <message>
        <source>Contribute</source>
        <translation>Đóng góp</translation>
    </message>
    <message>
        <source>Steam Workshop</source>
        <translation>Steam Workshop</translation>
    </message>
    <message>
        <source>Issue Tracker</source>
        <translation type="unfinished">Issue Tracker</translation>
    </message>
    <message>
        <source>Reddit</source>
        <translation type="unfinished">Reddit</translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <source>Open in browser</source>
        <translation>Mở trong trình duyệt</translation>
    </message>
</context>
<context>
    <name>CreateSidebar</name>
    <message>
        <source>Tools Overview</source>
        <translation type="unfinished">Tools Overview</translation>
    </message>
    <message>
        <source>Video Import h264 (.mp4)</source>
        <translation type="unfinished">Video Import h264 (.mp4)</translation>
    </message>
    <message>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation type="unfinished">Video Import VP8 &amp; VP9 (.webm)</translation>
    </message>
    <message>
        <source>Video import (all types)</source>
        <translation type="unfinished">Video import (all types)</translation>
    </message>
    <message>
        <source>GIF Wallpaper</source>
        <translation type="unfinished">GIF Wallpaper</translation>
    </message>
    <message>
        <source>QML Wallpaper</source>
        <translation type="unfinished">QML Wallpaper</translation>
    </message>
    <message>
        <source>HTML5 Wallpaper</source>
        <translation type="unfinished">HTML5 Wallpaper</translation>
    </message>
    <message>
        <source>Website Wallpaper</source>
        <translation type="unfinished">Website Wallpaper</translation>
    </message>
    <message>
        <source>QML Widget</source>
        <translation type="unfinished">QML Widget</translation>
    </message>
    <message>
        <source>HTML Widget</source>
        <translation type="unfinished">HTML Widget</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <source>Import any video type</source>
        <translation>Nhập bất kì loại video</translation>
    </message>
    <message>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation>Tùy thuộc vào cấu hình của PC mà việc chuyển đổi ảnh động của bạn sang một dạng video khác sẽ tốt hơn. Nếu cả hai đều có hiệu năng kém thì bạn có thể dùng một ảnh động QML!. Các loại video hỗ trợ:

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</translation>
    </message>
    <message>
        <source>Set your preffered video codec:</source>
        <translation>Chọn loại video mà bạn muốn:</translation>
    </message>
    <message>
        <source>Quality slider. Lower value means better quality.</source>
        <translation>Chất lượng video: Giá trị thấp hơn đồng nghĩa với việc chất lượng cao hơn.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Mở tài liệu tham khảo</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Chọn tệp</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <source>An error occurred!</source>
        <translation>Đã có lỗi xảy ra!</translation>
    </message>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Sao chép vào khay nhớ tạm</translation>
    </message>
    <message>
        <source>Back to create and send an error report!</source>
        <translation>Quay trở lại việc tạo ảnh động và gửi một báo cáo lỗi!</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Đang tạo ra ảnh xem trước...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Đang tạo ra hình thu nhỏ xem trước...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Đang tao ra video 5 giây xem trước...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Đang tạo ra gif xem trước...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Đang chuyển đổi dạng âm thanh...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Đang chuyển đổi dạng video... Việc này có thể tốn kha khá thời gian!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Đã có lỗi xảy ra khi chuyển đổi dạng video!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Đã có lỗi xảy ra khi đang xử lý video!</translation>
    </message>
    <message>
        <source>Convert a video to a wallpaper</source>
        <translation>Chuyển đổi một video sang ảnh động</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Đang tạo ra video xem trước...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Tên (bắt buộc!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Mô tả</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>Link YouTube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Hủy bỏ</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Lưu</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Lưu hình nền...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <source>Volume</source>
        <translation>Âm lượng</translation>
    </message>
    <message>
        <source>Current Video Time</source>
        <translation>Thời gian video hiện tại</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Cách lấp đầy</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Kéo dài</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Lấp đầy</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Chứa đựng</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Bao phủ</translation>
    </message>
    <message>
        <source>Scale_Down</source>
        <translation>Giảm tỉ lệ</translation>
    </message>
</context>
<context>
    <name>ExitPopup</name>
    <message>
        <source>Minimize ScreenPlay</source>
        <translation type="unfinished">Minimize ScreenPlay</translation>
    </message>
    <message>
        <source>Always minimize ScreenPlay</source>
        <translation type="unfinished">Always minimize ScreenPlay</translation>
    </message>
    <message>
        <source>You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</source>
        <translation type="unfinished">You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</translation>
    </message>
    <message>
        <source>Quit ScreenPlay now</source>
        <translation type="unfinished">Quit ScreenPlay now</translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <source>Import a Gif Wallpaper</source>
        <translation>Nhập một ảnh động gif</translation>
    </message>
    <message>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation>Thả một tệp *.gif vào đây hoặc nhấn nút &apos;Chọn một tệp&apos; ở dưới đây.</translation>
    </message>
    <message>
        <source>Select your gif</source>
        <translation>Chọn gif của bạn</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Tên ảnh động</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Đươc tạo bởi</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Thẻ</translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <source>Create a HTML Wallpaper</source>
        <translation>Tạo một ảnh động HTML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Tên ảnh động</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Được tạo bởi</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Mô tả</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Bản quyền &amp; Thẻ</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Ảnh xem trước</translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <source>Create a HTML widget</source>
        <translation>Tạo một widget HTML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Tên widget</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Đươc tạo bởi</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Thẻ</translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>AnalyseVideo...</source>
        <translation>Đang xử lý video...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Đang tạo ra ảnh xem trước...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Đang tạo ra hình thu nhỏ xem trước...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Đang tao ra video 5 giây xem trước...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Đang tạo ra gif xem trước...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Đang chuyển đổi dạng âm thanh...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Đang chuyển đổi dạng video... Việc này có thể tốn kha khá thời gian!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Đã có lỗi xảy ra khi chuyển đổi dạng video!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Đã có lỗi xảy ra khi đang xử lý video!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation>Nhập một video vào hình nền</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Đang tạo ra video xem trước...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Tên (bắt buộc!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Mô tả</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>Link YouTube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Hủy bỏ</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Lưu</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Lưu hình nền...</translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <source>Import a .webm video</source>
        <translation>Nhập một tệp webm</translation>
    </message>
    <message>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation>Khi mà nhập tệp webm chúng ta có thể bỏ qua quá trình chuyển đổi. Khi mà bạn có được kết quả không thỏa mãn với trình nhập tệp tất cả các loại video của ScreenPlay thì bạn cũng có thể sử dụng công cụ mã nguồn mở HandBrake!</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation>Loại tệp không hợp lệ. Bắt buộc phải là tệp VP8 hoặc VP9 hợp lệ (*.webm)!</translation>
    </message>
    <message>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation>Thả một tệp *.webm vào đây hoặc nhấn nút &apos;Chọn một tệp&apos; ở dưới đây.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Mở tài liệu tham khảo</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Chọn một tệp</translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <source>AnalyseVideo...</source>
        <translation type="unfinished">AnalyseVideo...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation type="unfinished">Generating preview image...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished">Generating preview thumbnail image...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished">Generating 5 second preview video...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation type="unfinished">Generating preview gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation type="unfinished">Converting Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished">Converting Video... This can take some time!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished">Converting Video ERROR!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished">Analyse Video ERROR!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation type="unfinished">Import a video to a wallpaper</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation type="unfinished">Generating preview video...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation type="unfinished">Name (required!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation type="unfinished">Youtube URL</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished">Abort</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished">Save</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation type="unfinished">Save Wallpaper...</translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <source>Import a .mp4 video</source>
        <translation type="unfinished">Import a .mp4 video</translation>
    </message>
    <message>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation type="unfinished">ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation type="unfinished">Invalid file type. Must be valid h264 (*.mp4)!</translation>
    </message>
    <message>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished">Drop a *.mp4 file here or use &apos;Select file&apos; below.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation type="unfinished">Open Documentation</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="unfinished">Select file</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Refreshing!</source>
        <translation>Đang làm mới!</translation>
    </message>
    <message>
        <source>Pull to refresh!</source>
        <translation>Kéo xuống để làm mới!</translation>
    </message>
    <message>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>Lấy thêm nhiều hình nền &amp; widgets từ Steam Workshop!</translation>
    </message>
    <message>
        <source>Open containing folder</source>
        <translation>Mở thư mục chứa hình nền.</translation>
    </message>
    <message>
        <source>Remove Item</source>
        <translation>Xóa hình nền</translation>
    </message>
    <message>
        <source>Remove via Workshop</source>
        <translation>Xóa nhờ Workshop</translation>
    </message>
    <message>
        <source>Open Workshop Page</source>
        <translation>Mở trang workshop</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this item?</source>
        <translation>Bạn có chắc chắn muốn xóa hình nền này không?</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished">Export</translation>
    </message>
    <message>
        <source>We only support adding one item at once.</source>
        <translation type="unfinished">We only support adding one item at once.</translation>
    </message>
    <message>
        <source>File type not supported. We only support &apos;.screenplay&apos; files.</source>
        <translation type="unfinished">File type not supported. We only support &apos;.screenplay&apos; files.</translation>
    </message>
    <message>
        <source>Import Content...</source>
        <translation type="unfinished">Import Content...</translation>
    </message>
    <message>
        <source>Export Content...</source>
        <translation type="unfinished">Export Content...</translation>
    </message>
</context>
<context>
    <name>InstalledNavigation</name>
    <message>
        <source>All</source>
        <translation type="unfinished">All</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="unfinished">Scenes</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="unfinished">Widgets</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>Lấy thêm nhiều hình nền &amp; widgets từ Steam Workshop</translation>
    </message>
    <message>
        <source>Browse the Steam Workshop</source>
        <translation>Duyệt qua Steam Workshop</translation>
    </message>
    <message>
        <source>Get content via our forum</source>
        <translation type="unfinished">Get content via our forum</translation>
    </message>
    <message>
        <source>Open the ScreenPlay forum</source>
        <translation type="unfinished">Open the ScreenPlay forum</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <source>Wallpaper Configuration</source>
        <translation>Cấu hình ảnh động</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>Xóa mục đã chọn</translation>
    </message>
    <message>
        <source>Wallpapers</source>
        <translation>Ảnh động</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widgets</translation>
    </message>
    <message>
        <source>Remove all </source>
        <translation type="unfinished">Remove all </translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <source>Set color</source>
        <translation>Đặt màu</translation>
    </message>
    <message>
        <source>Please choose a color</source>
        <translation>Xin hãy chọn một màu</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <source>Create</source>
        <translation type="unfinished">Create</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation type="unfinished">Workshop</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation type="unfinished">Installed</translation>
    </message>
    <message>
        <source>Community</source>
        <translation type="unfinished">Community</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished">Settings</translation>
    </message>
    <message>
        <source>Mute/Unmute all Wallpaper</source>
        <translation type="unfinished">Mute/Unmute all Wallpaper</translation>
    </message>
    <message>
        <source>Pause/Play all Wallpaper</source>
        <translation type="unfinished">Pause/Play all Wallpaper</translation>
    </message>
    <message>
        <source>Configure Wallpaper</source>
        <translation type="unfinished">Configure Wallpaper</translation>
    </message>
    <message>
        <source>Support me on Patreon!</source>
        <translation type="unfinished">Support me on Patreon!</translation>
    </message>
    <message>
        <source>Close All Content</source>
        <translation type="unfinished">Close All Content</translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <source>Create a QML Wallpaper</source>
        <translation>Tạo một ảnh động QML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Tên ảnh động</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Đươc tạo bởi</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Mô tả</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Bản quyền &amp; Thẻ</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Ảnh xem trước</translation>
    </message>
</context>
<context>
    <name>QMLWallpaperMain</name>
    <message>
        <source>My ScreenPlay Wallpaper 🚀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <source>Create a QML widget</source>
        <translation>Tạo một widget QML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Tên widget</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Đươc tạo bởi</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Thẻ</translation>
    </message>
</context>
<context>
    <name>QMLWidgetMain</name>
    <message>
        <source>My Widget 🚀</source>
        <translation type="unfinished">My Widget 🚀</translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <source>Profile saved successfully!</source>
        <translation>Đã lưu cấu hình thành công!</translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>NEW</source>
        <translation>MỚI</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>Tự động mở</translation>
    </message>
    <message>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation>ScreenPlay sẽ chạy cùng với Windows và sẽ thiết lập màn hình nền cho bạn.</translation>
    </message>
    <message>
        <source>High priority Autostart</source>
        <translation>Tự động mở ưu tiên hơn</translation>
    </message>
    <message>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation>Tùy chọn này cho phép ScreenPlay quyền tự động chạy ưu tiên hơn những ứng dụng khác.</translation>
    </message>
    <message>
        <source>Send anonymous crash reports and statistics</source>
        <translation>Gửi báo cáo sự cố và só liệu thống kê ẩn danh</translation>
    </message>
    <message>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation>Giúp chúng tôi làm ScreenPlay trở nên nhanh và ổn định hơn. Tất cả các dữ liệu thu thập được đều là ẩn danh và chỉ được sử dụng cho mục đích phát triển! Chúng tôi dùng &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; để thu thập và xử lý dữ liệu này. Một &lt;b&gt;sự cảm ơn lớn dành cho họ&lt;/b&gt; vì đã cung cấp cho chúng tôi bản trả phí miễn phí cho những dự án mã nguồn mở!</translation>
    </message>
    <message>
        <source>Set save location</source>
        <translation>Đặt vị trí lưu</translation>
    </message>
    <message>
        <source>Set location</source>
        <translation>Đặt vị trí</translation>
    </message>
    <message>
        <source>Your storage path is empty!</source>
        <translation>Đường dẫn lưu trữ của bạn đang trống!</translation>
    </message>
    <message>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation>Quan trọng: Thay đổi thư mục này không có hiệu ứng gì ở thư mục tải về của workshop. ScreenPlay chỉ hỗ trợ có một thư mục chứa nội dung!</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Ngôn ngữ</translation>
    </message>
    <message>
        <source>Set the ScreenPlay UI Language</source>
        <translation>Đặt ngôn ngữ của ScreenPlay</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Chủ đề</translation>
    </message>
    <message>
        <source>Switch dark/light theme</source>
        <translation>Chuyển chủ để sáng/tôí</translation>
    </message>
    <message>
        <source>System Default</source>
        <translation>Mặc định theo hệ thống</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Tối</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Sáng</translation>
    </message>
    <message>
        <source>Performance</source>
        <translation>Hiệu suất</translation>
    </message>
    <message>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation>Tạm dừng ảnh nền video khi có một ứng dụng khác đang mở phía trước</translation>
    </message>
    <message>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation>Chúng tôi tắt hiển thị video (không phải âm thanh!) Để có hiệu suất tốt nhất. Nếu bạn gặp sự cố, bạn có thể vô hiệu hóa hành vi này tại đây. Yêu cầu khởi động lại hình nền!</translation>
    </message>
    <message>
        <source>Default Fill Mode</source>
        <translation>Cách lấp đầy mặc định</translation>
    </message>
    <message>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation>Đặt thuộc tính này để xác định cách chia tỷ lệ video để phù hợp với khu vực mục tiêu.</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Kéo dài</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Lấp đầy</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Chứa đựng</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Bao phủ</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Giảm tỉ lệ</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Về ứng dụng</translation>
    </message>
    <message>
        <source>Thank you for using ScreenPlay</source>
        <translation>Cảm ơn bạn vì đã sử dụng ScreenPlay</translation>
    </message>
    <message>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation>Chào, tôi là Elias Steurer hay được biết đến là Kelteseth và tôi là người phát triển của ScreenPlay. Cảm ơn bạn đã sử dụng phần mềm của tôi. Bạn có thể theo dõi tôi để nhận được những cập nhật về ScreenPlay tại:</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Phiên bản</translation>
    </message>
    <message>
        <source>Open Changelog</source>
        <translation>Mở nhật kí thay đổi</translation>
    </message>
    <message>
        <source>Third Party Software</source>
        <translation>Phần mềm của bên thứ ba</translation>
    </message>
    <message>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation>ScreenPlay sẽ không thể có được nếu như không có thành quả của những người khác. Một lời cảm ơn lớn đến: </translation>
    </message>
    <message>
        <source>Licenses</source>
        <translation>Bản quyền</translation>
    </message>
    <message>
        <source>Logs</source>
        <translation>Nhật kí</translation>
    </message>
    <message>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation>Nếu ScreenPlay của bạn hoạt động sai thì đây là một cách tốt để tìm câu trả lời. Ở đây hiện tất cả các nhật kí và cảnh báo trong khi chạy</translation>
    </message>
    <message>
        <source>Show Logs</source>
        <translation>Hiện nhật kí</translation>
    </message>
    <message>
        <source>Data Protection</source>
        <translation>Bảo vệ dữ liệu</translation>
    </message>
    <message>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation>Chúng tôi sử dụng dữ liệu của bạn rất cẩn thận để cải thiện ScreenPlay. Chúng tôi không bán hoặc chia sẻ thông tin này (ẩn danh) với người khác!</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation>Quyền riêng tư</translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Sao chép vào khay nhớ tạm</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <source>Set Wallpaper</source>
        <translation>Đặt hình nền</translation>
    </message>
    <message>
        <source>Set Widget</source>
        <translation>Đặt widget</translation>
    </message>
    <message>
        <source>Headline</source>
        <translation>Tiêu đề</translation>
    </message>
    <message>
        <source>Select a Monitor to display the content</source>
        <translation>Chọn một màn hình để hiển thị nội dung</translation>
    </message>
    <message>
        <source>Set Volume</source>
        <translation>Chỉnh âm lượng</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Cách lấp đầy</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Kéo dài</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Lấp đầy</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Chứa đựng</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Bao phủ</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Giảm tỉ lệ</translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <source>Free tools to help you to create wallpaper</source>
        <translation type="unfinished">Free tools to help you to create wallpaper</translation>
    </message>
    <message>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation type="unfinished">Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <source>Create a Website Wallpaper</source>
        <translation>Tạo một hình nền của trang web</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Tên ảnh động</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Đươc tạo bởi</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Mô tả</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Thẻ</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Ảnh xem trước</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <source>Save</source>
        <translation>Lưu</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation>Đang lưu...</translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <source>News &amp; Patchnotes</source>
        <translation>Tin tức &amp; Ghi chú bản vá</translation>
    </message>
</context>
</TS>
